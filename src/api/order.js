import request from '@/utils/request'

export function fetchOrderList(params, data) {
  return request({
    url: '/apiOrder/list',
    method: 'post',
    params: params,
    data
  })
}
export function findOrderById(params) {
  return request({
    url: '/apiOrder/' + params,
    method: 'get'
  })
}
export function saveOrderList(data) {
  return request({
    url: '/apiOrder/saveOrder',
    method: 'put',
    data
  })
}

export function updateOrderList(data) {
  return request({
    url: '/apiOrder/updateOrder',
    method: 'put',
    data
  })
}

export function updateOrderInfo(data) {
  return request({
    url: '/apiOrder/updateOrderInfo',
    method: 'put',
    data
  })
}

export function findAllCustomerList(data) {
  return request({
    url: '/apiCustomer/alllist',
    method: 'get'
  })
}

export function findAllProductList(data) {
  return request({
    url: '/apiProduct/alllist',
    method: 'get'
  })
}
