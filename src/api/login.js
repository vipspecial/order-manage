import request from '@/utils/request'


export function login(username, password) {
  return request({
    url: '/login/' + username + '/' + password,
    method: 'get'
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}
